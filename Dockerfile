FROM node:14-alpine as builder

WORKDIR /app

COPY . .

RUN npm config set registry https://registry.npm.taobao.org

RUN npm i

RUN npm run build:prod

FROM nginx:latest

WORKDIR /app

COPY --from=builder /app/dist ./dist

RUN rm -rf /usr/share/nginx/html

RUN mv dist /usr/share/nginx/html

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]