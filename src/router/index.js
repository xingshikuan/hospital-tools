import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/* Router Modules */

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    noCache: true                if set true, the page will no be cached(default is false)
    affix: true                  if set true, the tag will affix in the tags-view
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/auth-redirect',
    component: () => import('@/views/login/auth-redirect'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/error-page/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/error-page/401'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    name: '数据大屏',
    children: [
      {
        path: 'dashboard',
        component: () => import('@/views/dashboard/admin'),
        name: 'Dashboard',
        meta: {
          title: '数据大屏',
          icon: 'shujudaping',
          affix: true
        }
      }
    ]
  },
  {
    path: '/profile',
    component: Layout,
    redirect: '/profile/index',
    hidden: true,
    children: [
      {
        path: 'index',
        component: () => import('@/views/profile/index'),
        name: 'Profile',
        meta: { title: 'Profile', icon: 'user', noCache: true }
      }
    ]
  }
]

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
export const asyncRoutes = [
  {
    path: '/api',
    component: Layout,
    redirect: '/api/index',
    name: '接口配置',
    meta: {
      title: '接口配置',
      roles: ['admin'],
      icon: 'exit-fullscreen'
    },
    children: [
      {
        path: 'index',
        component: () => import('@/views/apiconfig'),
        name: 'ApiConfig',
        meta: { title: '接口配置', icon: 'jiekoupeizhi' }
      }
    ]
  },
  {
    path: '/board',
    component: Layout,
    redirect: '/board/index',
    name: '数据看板',
    children: [
      {
        path: 'index',
        component: () => import('@/views/board'),
        name: 'Board',
        meta: { title: '数据看板', icon: 'shujukanban' }
      }
    ]
  },
  // {
  //   path: '/report',
  //   component: Layout,
  //   redirect: '/report/index',
  //   name: '数据报表',
  //   children: [
  //     {
  //       path: 'index',
  //       component: () => import('@/views/report'),
  //       name: 'Report',
  //       meta: { title: '数据报表', icon: 'shujubaobiao' }
  //     }
  //   ]
  // },
  {
    path: '/config',
    component: Layout,
    redirect: '/config/index',
    name: '配置中心',
    meta: {
      title: '配置中心',
      roles: ['admin'],
      icon: 'peizhizhongxin'
    },
    children: [
      {
        path: 'attribute',
        component: () => import('@/views/attribute'),
        name: 'attribute',
        meta: {
          title: '基础配置',
          roles: ['admin'],
          icon: "jichupeizhi"
        }
      },
      {
        path: 'user',
        component: () => import('@/views/user'),
        name: 'user',
        meta: {
          title: '用户配置',
          roles: ['admin'],
          icon: "yonghupeizhi"
          // if do not set roles, means: this page does not require permission
        }
      },
      {
        path: 'dashboardedit',
        component: () => import('@/views/dashboard/editor'),
        name: 'DashboardEdit',
        meta: {
          title: '大屏编辑',
          roles: ['admin'],
          icon: 'dapingbianji'
        }
      }
    ]
  },
  // {
  //   path: '/permission',
  //   component: Layout,
  //   redirect: '/permission/page',
  //   name: 'Permission',
  //   alwaysShow: true,
  //   meta: {
  //     title: 'Permission',
  //     icon: 'lock',
  //     roles: ['admin', 'editor'] // you can set roles in root nav
  //   },
  //   children: [
  //     {
  //       path: 'page',
  //       component: () => import('@/views/permission/page'),
  //       name: 'PagePermission',
  //       meta: {
  //         title: 'Page Permission',
  //         roles: ['admin'] // or you can only set roles in sub nav
  //       }
  //     },
  //     {
  //       path: 'directive',
  //       component: () => import('@/views/permission/directive'),
  //       name: 'DirectivePermission',
  //       meta: {
  //         title: 'Directive Permission'
  //         // if do not set roles, means: this page does not require permission
  //       }
  //     },
  //     {
  //       path: 'role',
  //       component: () => import('@/views/permission/role'),
  //       name: 'RolePermission',
  //       meta: {
  //         title: 'Role Permission',
  //         roles: ['admin']
  //       }
  //     }
  //   ]
  // },
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
