import $store from '@/store'
const colors= {
    light: {
        // echarts图表标题字体颜色
        font_color_echarts_title: '#222',
        // echarts图表文本颜色
        font_color_echarts_text: '#666',
        // echarts x y 轴分割线颜色
        font_color_echarts_line: '#dcdfe6',
        // echarts 图表背景颜色
        background_color_echarts: '#fff',
    },
    dark: {
        // echarts图表标题字体颜色
        font_color_echarts_title: '#fff',
        // echarts图表文本颜色
        font_color_echarts_text: "rgba(255,255,255,0.6)",
        // echarts x y 轴分割线颜色
        font_color_echarts_line: '#686868',
        // echarts 图表背景颜色
        background_color_echarts: '#152031',
    }
}

/** 获取颜色值 */
export function getThemeColor(key,theme) { 
    // const theme = $store.getters.customTheme||'light';

    return (colors[theme] || {})[key]
}