import axios from 'axios'
import { MessageBox, Message, Loading } from 'element-ui'
import store from '@/store'
import { getToken, removeToken } from '@/utils/auth'

let requestCount = 0 // 请求个数
let loadingInstance = null // loading实例数

// create an axios instance
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 50000 // request timeout
})

// request interceptor
service.interceptors.request.use(
  config => {
    // do something before request is sent
    if (requestCount == 0) {
      loadingInstance = Loading.service({
        lock: false,
        target: document.querySelector('.base-popup-content'),
        text: "加载中...",
        background: "rgba(0,0,0,0)"
      })
    }
    // 每次请求计数+1
    requestCount += 1

    if (store.getters.token) {
      // let each request carry token
      // ['X-Token'] is a custom headers key
      // please modify it according to the actual situation
      config.headers.Authorization = `Bearer ${getToken()}`
    }
    return config
  },
  error => {
    // do something with request error
    requestCount -= 1
    // 当加载实例存在且计数小于等于0的时候把加载效果关闭
    if (requestCount <= 0 && loadingInstance) {
      requestCount = 0
      loadingInstance.close()
    }
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
  */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  response => {
    // 当请求返回的时候计数-1
    requestCount -= 1
    // 当加载实例存在且计数小于等于0的时候把加载效果关闭
    if (requestCount <= 0 && loadingInstance) {
      requestCount = 0
      loadingInstance.close()
    }
    const res = response.data
    if (response.request.responseURL.indexOf('download') > -1) {
      return res
    }
    // if the custom code is not 20000, it is judged as an error.
    if (res.code !== 20000) {
      Message({
        message: res.message || 'Error',
        type: 'error',
        duration: 5 * 1000
      })

      // 50008: Illegal token; 50012: Other clients logged in; 50014: Token expired;
      if (res.code === 50008 || res.code === 50012 || res.code === 50014) {
        // to re-login
        MessageBox.confirm('You have been logged out, you can cancel to stay on this page, or log in again', 'Confirm logout', {
          confirmButtonText: 'Re-Login',
          cancelButtonText: 'Cancel',
          type: 'warning'
        }).then(() => {
          store.dispatch('user/resetToken').then(() => {
            location.reload()
          })
        })
      }
      return Promise.reject(new Error(res.message || 'Error'))
    } else {
      return res
    }
  },
  error => {
    /***** 接收到异常响应的处理开始 *****/
    // 当响应出现异常的时候计数-1
    requestCount -= 1
    // 当加载实例存在且计数小于等于0的时候把加载效果关闭
    if (requestCount <= 0 && loadingInstance) {
      requestCount = 0
      loadingInstance.close()
    }
    if (error.response.status === 401) {
      // 跳转到登录页面
      router.push('/login');
    }
    return Promise.reject(error)
  }
)

export default service
