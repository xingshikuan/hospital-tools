import request from '@/utils/request'

export function fetchBoardList(query) {
  return request({
    url: '/board/list',
    method: 'get',
    params: query
  })
}

export function deleteBoard(id) {
  return request({
    url: '/board/delete',
    method: 'delete',
    params: { id }
  })
}

export function updateBoardStatus(id, publish) {
  return request({
    url: '/board/publish',
    method: 'post',
    data: { id, publish }
  })
}

export function getBoardData(id, params) {
  return request({
    url: '/board/getData',
    method: 'post',
    data: { id, params }
  })
}

export function handleBoard(data) {
  return request({
    url: '/board/handle',
    method: 'post',
    data
  })
}

export function getOneBoard(id) {
  return request({
    url: '/board/one',
    method: 'get',
    params: { id }
  })
}

export function queryBoardList(term) {
  return request({
    url: '/board/query',
    method: 'get',
    params: { term }
  })
}
