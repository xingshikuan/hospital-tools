import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/api/list',
    method: 'get',
    params: query
  })
}

export function queryList(term) {
  return request({
    url: '/api/query',
    method: 'get',
    params: { term }
  })
}

export function deleteApi(id) {
  return request({
    url: '/api/delete',
    method: 'delete',
    params: { id }
  })
}

export function updateUseStatus(id, isUsed) {
  return request({
    url: '/api/isused',
    method: 'post',
    data: { id, isUsed }
  })
}

export function handleApi(data) {
  return request({
    url: '/api/handle',
    method: 'post',
    data
  })
}

export function getOneApi(id) {
  return request({
    url: '/api/one',
    method: 'get',
    params: {id}
  })
}