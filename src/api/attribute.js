import request from '@/utils/request'

export function getAttribute(key) {
  return request({
    url: '/attribute/get',
    method: 'get',
    params: { key }
  })
}

export function attributeList(query) {
  return request({
    url: '/attribute/list',
    method: 'get',
    params: query
  })
}

export function handleAttribute(attribute) {
  return request({
    url: '/attribute/handle',
    method: 'post',
    data: attribute
  })
}


export function deleteApi(id) {
  return request({
    url: '/attribute/delete',
    method: 'delete',
    params: { id }
  })
}