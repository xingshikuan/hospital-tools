import request from '@/utils/request'

export function dashboardNow(type) {
  return request({
    url: '/dashboard/getNow',
    method: 'get',
    params:{type}
  })
}

export function saveDashboardNow(type,data) {
  return request({
    url: '/dashboard/save',
    method: 'post',
    data: {type,data}
  })
}

