import request from '@/utils/request'

export function templateList() {
  return request({
    url: '/report/template/list',
    method: 'get'
  })
}

export function reportList() {
  return request({
    url: '/report/list',
    method: 'get'
  })
}

export function generateReport(filename) {
  return request({
    url: '/report/generate/'+filename,
    method: 'get'
  })
}

export function downloadReport(filename) {
  return request({
    url: '/report/download/'+filename,
    method: 'GET',
    responseType: 'blob'
  })
}