import request from '@/utils/request'

export function fetchAuditList(query) {
  return request({
    url: '/audit/list',
    method: 'get',
    params: query
  })
}
