import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/user/login',
    method: 'post',
    data
  })
}

export function getInfo() {
  return request({
    url: '/user/info',
    method: 'get'
  })
}

export function logout() {
  return request({
    url: '/user/logout',
    method: 'get'
  })
}


export function userList(query) {
  return request({
    url: '/user/list',
    method: 'get',
    params: query
  })
}



export function handleUser(data) {
  return request({
    url: '/user/handle',
    method: 'post',
    data
  })
}

export function resetPassword(id, password) {
  return request({
    url: '/user/reset/password',
    method: 'post',
    data: { id, password }
  })
} 