import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/api/list',
    method: 'get',
    params: query
  })
}

export function deleteApi(id) {
  return request({
    url: '/api/delete',
    method: 'delete',
    params: { id }
  })
}

export function updateUseStatus(id, isUsed) {
  return request({
    url: '/api/isused',
    method: 'post',
    data: { id, isUsed }
  })
}

export function handleApi(data) {
  return request({
    url: '/api/handle',
    method: 'post',
    data
  })
}

export function fetchArticle(id) {
  return request({
    url: '/vue-element-admin/article/detail',
    method: 'get',
    params: { id }
  })
}

export function fetchPv(pv) {
  return request({
    url: '/vue-element-admin/article/pv',
    method: 'get',
    params: { pv }
  })
}

export function createArticle(data) {
  return request({
    url: '/vue-element-admin/article/create',
    method: 'post',
    data
  })
}

export function updateApi(data) {
  return request({
    url: '/vue-element-admin/article/update',
    method: 'post',
    data
  })
}
