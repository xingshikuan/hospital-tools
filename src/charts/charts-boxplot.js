import _ from 'lodash'
import { colorBar, colorsBase } from './color'
import { getThemeColor } from '@/styles/variables'
export function handleDataBoxplot(datas, matchs) {
  let result = {
    legend: [],
    series: [],
    xAxis: [],
  }
  let legend = {}
  let series = {}
  for (let match of matchs) {
    for (let row of datas[match.apiId].data) {
      let value = _.get(row, match.from);
      if (match.name == 'xAxis' && !result.xAxis.includes(value)) {
        result.xAxis.push(value)
      } else if (match.name == 'series') {
        if (_.has(legend, match.to)) {
          series[match.to].data.push(value)
        } else {
          legend[match.to] = true
          series[match.to] = {
            name: match.to,
            data: [value]
          }
        }
      }
    }
  }
  result.legend = _.keys(legend)
  result.series = _.toArray(series)
  return result
}

export function generateBoxplot(title, datas, matchs, theme) {
  const data = handleDataBoxplot(datas, matchs)

  const option = {
    backgroundColor: getThemeColor('background_color_echarts', theme),
    color: colorsBase,
    title: [
      {
        text: title,
        left: 'center',
        textStyle: {
          color: getThemeColor('font_color_echarts_title', theme)
        }
      },
    ],
    toolbox: {
      feature: {
        saveAsImage: {}
      }
    },
    legend: {
      show:false,
      data: data.legend,
      textStyle: {
        color: getThemeColor('font_color_echarts_text', theme)
      }
    },
    toolbox: {
      feature: {
        saveAsImage: {}
      }
    },
    dataset: [
      ...data.series.map(_ => {
        return {
          source: _.data
        }
      }),
      ...data.series.map((_, i) => {
        return {
          fromDatasetIndex: i,
          transform: {
            type: "boxplot",
            config: {
              itemNameFormatter(index) {
                return `${data.xAxis[index.value]}`
              }
            }
          }
        }
      }),
      {
        transform: {
          type: 'boxplot',
          config: {
            itemNameFormatter(index) {
              return `${data.legend[index.value]}`
            }
          }
        }
      },

    ],
    tooltip: {
      trigger: 'item',
      axisPointer: {
        type: 'shadow'
      },
      formatter: (params) => {
        let txt = '';
        txt += params.seriesName + '<br>'
          + params.marker + '上限值：' + params.data[5] + '%' + '<br>'
          + params.marker + '上四分位数：' + params.data[4] + '%' + '<br>'
          + params.marker + '中位数：' + params.data[3] + '%' + '<br>'
          + params.marker + '下四分位数:' + params.data[2] + '%' + '<br>'
          + params.marker + '下限值：' + params.data[1] + '%' + '<br>'
        return txt

      }
    },
    dataZoom: [
      {
        type: 'inside',
        start: 0,
        end: 20
      },
      {
        show: true,
        type: 'slider',
        top: '90%',
        xAxisIndex: [0],
        start: 0,
        end: 20
      }
    ],
    grid: {
      left: '10%',
      right: '10%',
      bottom: '15%'
    },
    xAxis: {
      type: 'category',
      boundaryGap: true,
      nameGap: 30,
      splitArea: {
        show: false
      },
      axisLabel: {
        textStyle: {
          color: getThemeColor('font_color_echarts_text', theme)
        }
      },
      splitLine: {
        lineStyle: {
          color: getThemeColor('font_color_echarts_line', theme)
        }
      },
      axisLine: {
        lineStyle: {
          color: getThemeColor('font_color_echarts_line', theme)
        }
      }
    },
    yAxis: {
      type: 'value',
      // name: 'km/s minus 299,000',
      splitArea: {
        show: false
      },
      axisLabel: {
        textStyle: {
          color: getThemeColor('font_color_echarts_text', theme)
        }
      },
      splitLine: {
        lineStyle: {
          color: getThemeColor('font_color_echarts_line', theme)
        }
      },
      axisLine: {
        lineStyle: {
          color: getThemeColor('font_color_echarts_line', theme)
        }
      }
    },
    series: [
      ...data.series.map((_, i) => {
        return {
          type: "boxplot",
          name: _.name,
          datasetIndex: data.series.length + i,
          itemStyle: {
            color:"rgba(0,0,0,0)"
          }
        }
      })
    ]
  };
  return option
}

// function drawBoxplot() {

//   const myChart = echarts.init(document.getElementById('boxplot'))
//   myChart.setOption(generateBoxplot('盒须图', data1.data, [
//     {
//       "name": "xAxis",
//       "from": "group",
//     },
//     {
//       "name": "series",
//       "from": "money",
//       "to": 'money'
//     },
//   ]))
// }
// drawBoxplot()