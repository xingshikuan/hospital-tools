import _ from 'lodash'
import { colorsBase, formatStackingArea } from './color';
import { getThemeColor } from '@/styles/variables';
export function handleDataStackingArea(datas, matchs) {
  let result = {
    legend: [],
    series: [],
    xAxis: [],
  }
  let legend = {}
  let series = {}

  let customColor = [];
  // 颜色索引
  let colorIndex = 0
  for (let match of matchs) {
    for (let row of datas[match.apiId].data) {
      let value = _.get(row, match.from);
      if (match.name == 'xAxis') {
        result.xAxis.push(value)
      } else if (match.name == 'series') {
        if (_.has(legend, match.to)) {
          series[match.to].data.push(value)
        } else {
          legend[match.to] = true
          series[match.to] = {
            name: match.to,
            type: 'line',
            areaStyle: {},
            // stack: 'Total',
            label: { show: true, position: "top" },
            emphasis: {
              focus: 'series'
            },
            data: [value]
          }

          if (match.color) {
            series[match.to].lineStyle = {
              color: match.color
            }
            series[match.to].color = match.color;
            customColor.push({ ...match, color: match.color })
          } else {
            series[match.to].lineStyle = {
              color: colorsBase[colorIndex % colorsBase.length]
            }
            series[match.to].color = colorsBase[colorIndex % colorsBase.length];
            customColor.push({ ...match, color: colorsBase[colorIndex % colorsBase.length] })
            colorIndex++;
          }
        }
      }
    }
  }
  result.legend = _.keys(legend)
  result.series = _.toArray(series)
  result.customColor = customColor

  return result
}

export function generateStackingArea(title, datas, matchs, theme) {
  let data = handleDataStackingArea(datas, matchs)
  const option = {
    backgroundColor: getThemeColor('background_color_echarts', theme),
    color: colorsBase,
    title: {
      text: title,
      textStyle: {
        color: getThemeColor('font_color_echarts_title', theme)
      }
    },
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        type: 'cross',
        label: {
          backgroundColor: '#6a7985'
        }
      }
    },
    legend: {
      data: data.legend,
      bottom:10,
      textStyle: {
        color: getThemeColor('font_color_echarts_text', theme)
      }
    },
    toolbox: {
      feature: {
        saveAsImage: {}
      }
    },
    grid: {
      top: '60px',
      left: '2%',
      right: '2%',
      bottom: '40px',
      containLabel: true
    },
    xAxis: {
      type: 'category',
      boundaryGap: false,
      data: data.xAxis,
      axisLabel: {
        textStyle: {
          color: getThemeColor('font_color_echarts_text', theme)
        }
      },
      splitLine: {
        lineStyle: {
          color: getThemeColor('font_color_echarts_line', theme)
        }
      },
      axisLine: {
        lineStyle: {
          color: getThemeColor('font_color_echarts_line', theme)
        }
      }
    },
    yAxis: {
      type: 'value',
      splitArea: { show: false },
      axisLabel: {
        textStyle: {
          color: getThemeColor('font_color_echarts_text', theme)
        }
      },
      splitLine: {
        lineStyle: {
          color: getThemeColor('font_color_echarts_line', theme)
        }
      },
      axisLine: {
        lineStyle: {
          color: getThemeColor('font_color_echarts_line', theme)
        }
      }
    },
    series: formatStackingArea(data.series, data.customColor),
    customColor: data.customColor
  }
  return option
}