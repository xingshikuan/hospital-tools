import _ from 'lodash'
import { colorBar, colorsBase } from './color'
import { getThemeColor } from '@/styles/variables';

export function handleDataScatter(datas, matchs) {
  let series = {};
  let legend = {};

  for (let match of matchs) {
    if (!series[match.to]) {
      legend[match.to] = true
      series[match.to] = {
        name: match.to,
        type: 'scatter',
        symbolSize: 20,
        emphasis: {
          focus: 'series'
        },
        data: _.get(datas[match.apiId].data, match.from),
        markArea: {
          silent: true,
          itemStyle: {
            color: '#eee'
          }
        }
      }
    }

    // console.log(datas[match.apiId].data)
    // for (let i = 0; i < datas[match.apiId].data.length; i++) {
    //   const item = datas[match.apiId].data[i]
      
    //   let value = _.get(item, match.from);
     
    //   if (!series[match.to].data[i]) { 
    //     series[match.to].data[i] = [];
    //   }
    //   if (match.name == 'xAxis') {
    //     series[match.to].data[i][0] =value
    //   } else if (match.name == 'yAxis') {
    //     series[match.to].data[i][1] = value
    //   }
    // }
  }
  return {
    series: _.toArray(series),
    legend: _.keys(legend)
  }
}

export function generateScatter(title, datas, matchs, theme) {
  let data = handleDataScatter(datas, matchs)
  const option = {
    backgroundColor: getThemeColor('background_color_echarts', theme),
    color: colorsBase,
    title: {
      text: title,
      left:"center",
      textStyle: {
        color: getThemeColor('font_color_echarts_title', theme)
      }
    },
    toolbox: {
      feature: {
        saveAsImage: {}
      }
    },
    grid: {
      top: '60px',
      left: '2%',
      right: '2%',
      bottom: '40px',
      containLabel: true
    },
    tooltip: {
      trigger: 'axis',
    },
    series: data.series,
    legend: {
      data: data.legend,
      left: 'center',
      bottom: 10,
      textStyle: {
        color: getThemeColor('font_color_echarts_text', theme)
      }
    },
    toolbox: {
      feature: {
        saveAsImage: {}
      }
    },
    xAxis: {
      splitArea: { show: false },
      splitLine: {
        show:false,
      },
      axisLabel: {
        textStyle: {
          color: getThemeColor('font_color_echarts_text', theme)
        }
      },
      // splitLine: {
      //   lineStyle: {
      //     color: getThemeColor('font_color_echarts_line', theme)
      //   }
      // },
      axisLine: {
        lineStyle: {
          color: getThemeColor('font_color_echarts_line', theme)
        }
      }
    },
    yAxis: {
      splitArea: { show: false },
      splitLine: {
        show:false,
      },
      axisLabel: {
        textStyle: {
          color: getThemeColor('font_color_echarts_text', theme)
        }
      },
      // splitLine: {
      //   lineStyle: {
      //     color: getThemeColor('font_color_echarts_line', theme)
      //   }
      // },
      axisLine: {
        lineStyle: {
          color: getThemeColor('font_color_echarts_line', theme)
        }
      }
    },
  }
  return option
}