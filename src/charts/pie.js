import _ from 'lodash'
import { colorsBase } from './color'
import { getThemeColor } from '@/styles/variables';
export function handleDataPie(datas, matchs) {
  const result = []
  for (const match of matchs) {

    for (let i = 0;i<datas[match.apiId].data.length;i++) {
      const value = _.get(datas[match.apiId].data[i], match.from)
      if (!result[i]) { 
        result[i] = {};
      }
      result[i][match.name] = value;
    }
  }

  return result
}

export function generatePie(title, datas, matchs, theme) {
  const data = handleDataPie(datas, matchs)
  const option = {
    backgroundColor: getThemeColor('background_color_echarts', theme),
    color: colorsBase,
    title: {
      text: title,
      left: 'center',
      textStyle: {
        color: getThemeColor('font_color_echarts_title', theme)
      }
    },
    tooltip: {
      trigger: 'item'
    },
    toolbox: {
      feature: {
        saveAsImage: {}
      }
    },
    legend: {
      orient: 'vertical',
      left: 'left',
      textStyle: {
        color: getThemeColor('font_color_echarts_text', theme)
      },
      // TODO:图例过多后显示样式（分页显示）
      type: 'scroll',
      pageIconColor: '#7f7f7f', //图例分页左右箭头图标颜色
      pageTextStyle: {
        color: '#7f7f7f', //图例分页页码的颜色设置
      },
      pageIconSize: 12,  //当然就是按钮的大小
      pageIconInactiveColor: '#7f7f7f',  // 禁用的按钮颜色
    },
    toolbox: {
      feature: {
        saveAsImage: {}
      }
    },
    series: [
      {
        type: 'pie',
        radius: '50%',
        center: ['60%', '50%'],
        data: data,
        emphasis: {
          itemStyle: {
            shadowBlur: 10,
            shadowOffsetX: 0,
            // shadowColor: 'rgba(0, 0, 0, 0.5)'
          }
        },
        label: {
          show:true,
          textStyle: {
            color: getThemeColor('font_color_echarts_text', theme)
          }
        }
      }
    ]
  }
  return option
}
