/** 基础颜色值 值为rgb 但是使用时需要 .map(_=>`${rgb(_)}`) */
export const colorsBase = [
    "#5EA2F5",
    "#FF886B",
    "#75DBE5",
    "#FCC665",
    "#6FCF8D",
    "#C0E457",
    "#9791FF",
    "#F38EDE",
];

/** 颜色-柱状 深色渐变 */
export const colorBar = [
    [colorsBase[0], "#97CFFB"],
    [colorsBase[1], "#FFBDA4"],
    [colorsBase[2], "#ADEFF4"],
    [colorsBase[3], "#FFE3B2"],
    [colorsBase[4], "#A8E9C1"],
    [colorsBase[5], "#E1F390"],
    [colorsBase[6], "#C8C3FF"],
    [colorsBase[7], "#FAC1F1"],
];

/** hex转rgba */
export const hex2Rgb = (hexValue, alpha = 1) => {
    const rgx = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    const hex = hexValue.replace(rgx, (m, r, g, b) => r + r + g + g + b + b);
    const rgb = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    if (!rgb) {
        return hexValue;
    }
    const r = parseInt(rgb[1], 16),
        g = parseInt(rgb[2], 16),
        b = parseInt(rgb[3], 16);
    return `rgba(${r},${g},${b},${alpha})`;
};

/** 格式化折线图线颜色
 * @param { any } series 数据
 */
export const formatLineColor = (series) => {
    return series.map((_, i) => {
        if (_.type != 'line') {
            return _;
        }
        // 点颜色
        _.color = hex2Rgb(colorsBase[i % colorsBase.length])
        // 线颜色
        _.lineStyle = {
            color: hex2Rgb(colorsBase[i % colorsBase.length])
        }
        return _
    })
}

/** 格式化折线图线颜色渐变 */
export const formatLineColorGradient = (series, from, to, customColor = []) => {
    return series.map((_, i) => {
        if (_.type != 'line') {
            return _;
        }




        // 查找自定义颜色中是否有当前数据
        const current = customColor.find(item => item.to == _.name)
        if (current) {
            let colorArr = [colorsBase[0], "#97CFFB"];
            for (let j = 0; j < colorBar.length; j++) {
                // 判断颜色是否相同
                if (colorBar[j][0] == current.color) {
                    colorArr = colorBar[j];
                    break;
                }
            }

            // 点颜色
            _.color = hex2Rgb(current.color)
            // 线颜色
            _.lineStyle = {
                color: {
                    type: 'linear', // 指定渐变类型
                    x: 0, // 起始颜色位置，取值范围[0, 1]
                    y: 0, // 结束颜色位置，取值范围[0, 1]
                    x2: 0, // x2 和 y2 指定渐变的方向，取值范围[0, 1]
                    y2: 1,
                    colorStops: [
                        {
                            offset: 0,
                            color: hex2Rgb(colorArr[0], from),
                        },
                        {
                            offset: 1,
                            color: hex2Rgb(colorArr[1], to),
                        },
                    ],
                },
            }
        } else {

            // 点颜色
            _.color = hex2Rgb(colorsBase[i % colorsBase.length])
            // 线颜色
            _.lineStyle = {
                color: {
                    type: 'linear', // 指定渐变类型
                    x: 0, // 起始颜色位置，取值范围[0, 1]
                    y: 0, // 结束颜色位置，取值范围[0, 1]
                    x2: 0, // x2 和 y2 指定渐变的方向，取值范围[0, 1]
                    y2: 1,
                    colorStops: [
                        {
                            offset: 0,
                            color: hex2Rgb(colorBar[
                                i % colorBar.length
                            ][0], from),
                        },
                        {
                            offset: 1,
                            color: hex2Rgb(colorBar[
                                i % colorBar.length
                            ][1], to),
                        },
                    ],
                },
            }
        }

        return _
    })
}

/** 格式化折线图
 * @param { any } series 数据
 */
export const formatStackingArea = (series, customColor = []) => {
    return series.map((_, i) => {
        if (_.type != 'line') {
            return _;
        }

        // 查找自定义颜色中是否有当前数据
        const current = customColor.find(item => item.to == _.name)
        if (current) {
            _.areaStyle = {
                color: {
                    type: "linear",
                    x: 0,
                    y: 1,
                    x2: 0,
                    y2: 0,
                    colorStops: [
                        {
                            offset: 0,
                            color: hex2Rgb(current.color, 0),
                        },
                        {
                            offset: 1,
                            color: hex2Rgb(current.color, 0.2),
                        },
                    ],
                },
            }
        } else {
            _.areaStyle = {
                color: {
                    type: "linear",
                    x: 0,
                    y: 1,
                    x2: 0,
                    y2: 0,
                    colorStops: [
                        {
                            offset: 0,
                            color: hex2Rgb(colorsBase[i % colorsBase.length], 0),
                        },
                        {
                            offset: 1,
                            color: hex2Rgb(colorsBase[i % colorsBase.length], 0.2),
                        },
                    ],
                },
            }
        }

        return _
    })
}


/** 柱状图
 * @param { any } series 数据
 * @param { string } position 方向 bt:上下 lr:左右
 */
export const formatBar = (series, position, customColor = []) => {
    return series.map((_, i) => {
        if (_.type != 'bar') {
            return _;
        }

        // 查找自定义颜色中是否有当前数据
        const current = customColor.find(item => item.to == _.name)
        if (current) {
            let colorArr = [colorsBase[0], "#97CFFB"];
            for (let j = 0; j < colorBar.length; j++) {
                // 判断颜色是否相同
                if (colorBar[j][0] == current.color) {
                    colorArr = colorBar[j];
                    break;
                }
            }

            _.itemStyle = {
                normal: {
                    color: {
                        type: "linear",
                        x: position == 'lr' ? 1 : 0,
                        y: 0,
                        x2: 0,
                        y2: position == 'bt' ? 1 : 0,
                        colorStops: [
                            {
                                offset: 0,
                                color: colorArr[0],
                            },
                            {
                                offset: 1,
                                color: colorArr[1],
                            },
                        ],
                    },
                    barBorderRadius: position == 'lr' ? [0, 4, 4, 0] : [4, 4, 0, 0] // 左上角和右上角添加圆角
                },
            };
        } else {
            _.itemStyle = {
                normal: {
                    color: {
                        type: "linear",
                        x: position == 'lr' ? 1 : 0,
                        y: 0,
                        x2: 0,
                        y2: position == 'bt' ? 1 : 0,
                        colorStops: [
                            {
                                offset: 0,
                                color: colorBar[
                                    i % colorBar.length
                                ][0],
                            },
                            {
                                offset: 1,
                                color: colorBar[
                                    i % colorBar.length
                                ][1],
                            },
                        ],
                    },
                    barBorderRadius: position == 'lr' ? [0, 4, 4, 0] : [4, 4, 0, 0] // 左上角和右上角添加圆角
                },
            };
        }
        return _;
    })
}
