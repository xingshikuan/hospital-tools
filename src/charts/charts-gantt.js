import _ from 'lodash'
import { colorBar, colorsBase } from './color'

// const stageTypeColor = {
//     before: "rgba(0,0,0,0.2)",
//     during: "",
//     done:"rgba(255,255,255,0.2)"
// }
export function handleDataGantt(datas, matchs, stageTypeColor) {
    let series = [];
    let index = 0;
    let legends = [];
    // let legends = {};
    // 原始数据（第一次数据格式化后数据）
    let rawData = [];

    for (let i = 0; i < matchs.length; i++) {
        const match = matchs[i];

        for (let j = 0; j < datas[match.apiId].data.length; j++) {
            const row = datas[match.apiId].data[j];

            if (!rawData[j]) {
                rawData[j] = {};
            }
            const value = _.get(row, match.from);

            if (match.name == 'hover') {
                if (!rawData[j].hover) {
                    rawData[j].hover = []
                }
                let hoverRow = rawData[j].hover.find(_ => _.label == match.to)
                if (hoverRow) {
                    hoverRow.value = value
                } else {
                    rawData[j].hover.push({
                        label: match.to,
                        value
                    })
                }
            } else if (match.name == 'icon') {
                if (!rawData[j].icon) {
                    rawData[j].icon = []
                }
                let iconRow = rawData[j].icon.find(_ => _.label == match.to)
                if (iconRow) {
                    iconRow.value = value
                } else {
                    rawData[j].icon.push({
                        label: match.to,
                        value
                    })
                }
            } else {
                rawData[j][match.name] = value;

            }


            if (match.name == 'type') {
                const legendRow = legends.find(_ => _.label == value)
                if (!legendRow) {
                    legends.push({
                        label: value,
                        color: colorsBase[index++]
                    })
                }
            }
        }
    }

    // 首先保证roomName去重
    series = [...new Set(rawData.map(_ => _.roomName))].map(_ => {
        return {
            roomName: _,
            time: []
        }
    });


    legends = legends.map(_ => {
        if (stageTypeColor[_.label]) {
            _.color = stageTypeColor[_.label];
        }
        return _;
    })

    rawData.forEach(_ => {
        // 查找到当前roomName 然后进行数据填充
        let roomRow = series.find(room => room.roomName == _.roomName);
        if (roomRow) {
            // _.serverTime 暂未处理

            // 手术类型
            // roomRow.type = _.type;
            // 查询组 如果有组则数据添加到组中 如果没有组数据则新增组
            let group = roomRow.time.find(times => times.groupId == _.groupId)
            if (group) {
                group._data.push({
                    ..._,
                    // 开始时间
                    beginTime: _.beginTime,
                    // 结束时间
                    endTime: _.endTime,
                    // 阶段类型 可选值：before(深色) during(默认) done(浅色)
                    stageTypeColor: stageTypeColor[_.stageType],
                    // 服务器当前时间
                    serverTime: _.serverTime,
                })
            } else {
                const legendRow = legends.find(legend => legend.label == _.type)
                roomRow.time.push({
                    groupId: _.groupId,
                    type: _.type,
                    color: legendRow?.color,
                    _data: [{
                        ..._,
                        // 开始时间
                        beginTime: _.beginTime,
                        // 结束时间
                        endTime: _.endTime,
                        // 阶段类型 可选值：before(深色) during(默认) done(浅色)
                        stageTypeColor: stageTypeColor[_.stageType],
                        // 服务器当前时间
                        serverTime: _.serverTime,
                    }]
                })
            }

            // 动用率
            roomRow.rate = _.rate
            // 服务器时间
            roomRow.serverTime = _.serverTime;
        }
    })



    return {
        legend: legends,
        series,
        matchs,
        type: "gantt"
    }

}

export function generateGantt(name, datas, matchs, stageTypeColor) {
    const data = handleDataGantt(datas, matchs, stageTypeColor)
    return data;
}

