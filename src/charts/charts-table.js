import _ from 'lodash'
import { colorBar, colorsBase } from './color'
export function handleDataTable(datas, matchs) {
    let series = [];
    let index = -1;
    let legends = {};
    // 表头
    let tableHead = [];

    /**
     * [
     *  { name: 'field', from: 'id',to:'编号' },
     *  { name: 'field', from: 'name',to:'名称' },
     * ]
     */
    for (let i = 0; i < matchs.length; i++) {
        const match = matchs[i];
        legends[match.to] = true;

        for (let j = 0; j < datas[match.apiId].data.length; j++) {
            const row = datas[match.apiId].data[j];

            if (!series[j]) {
                series[j] = {

                };
            }

            series[j][match.to] = _.get(row, match.from);
        }
    }
    return {
        legend: _.keys(legends),
        series,
        matchs,
        type: "table"
    }
}

export function generateTable(title, datas, matchs) {
    const data = handleDataTable(datas, matchs)

    return data
}

