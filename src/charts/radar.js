import _ from 'lodash'
import { colorsBase } from './color'
import { getThemeColor } from '@/styles/variables'
export function handleDataRadar(datas, matchs) {
  let result = {
    legend: [],
    series: [],
    xAxis: [],
  }
  let legend = {}
  let series = {}
  for (let match of matchs) {
    for (let row of datas[match.apiId].data) {
      let value = _.get(row, match.from);
      if (match.name == 'xAxis' && result.xAxis.indexOf(value) == -1) {
        result.xAxis.push(value)
      } else if (match.name == 'series') {
        if (_.has(legend, match.to)) {
          series[match.to].value.push(value)
        } else {
          legend[match.to] = true
          series[match.to] = {
            name: match.to,
            value: [value]
          }
        }
      }
    }
  }
  result.legend = _.keys(legend)
  result.series = _.toArray(series)
  return result
}

export function generateRadar(title, datas, matchs, theme) {
  const data = handleDataRadar(datas, matchs)

  const option = {
    backgroundColor: getThemeColor('background_color_echarts', theme),
    color: colorsBase,
    title: {
      text: title,
      left:"center",
      textStyle: {
        color: getThemeColor('font_color_echarts_title', theme)
      }
    },
    toolbox: {
      feature: {
        saveAsImage: {}
      }
    },
    legend: {
      data: data.legend,
      bottom:10,
      textStyle: {
        color: getThemeColor('font_color_echarts_text', theme)
      }
    },
    toolbox: {
      feature: {
        saveAsImage: {}
      }
    },
    radar: {
      //雷达图半径
      radius: '50%',
      center: ['50%', '50%'],
      // shape: 'circle',
      indicator: data.xAxis.map(_ => {
        return { name: _ }
      })
    },
    series: [
      {
        type: 'radar',
        data: data.series
      }
    ]
  }
  return option
}
