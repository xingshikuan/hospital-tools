import _ from 'lodash'
import { formatBar } from './color'
import { getThemeColor } from '@/styles/variables';

const labelStyle = {
  color: '#000000'
}


export function handleDataBarNegative(datas, matchs) {
  const result = {
    series: [],
    yAxis: []
  }
  let series = {};

  for (const match of matchs) {
    for (const row of datas[match.apiId].data) {
      let value = _.get(row, match.from)
      if (match.name === 'yAxis') {
        result.yAxis.push(value)
      } else if (match.name === 'series') {

        value = _.toNumber(value)

        if (!series[match.to]) {
          series[match.to] = {
            name: match.to,
            type: 'bar',
            barMaxWidth: '16px',
            data: [],
            label: {
              show: false,
              position: 'inside',
              color:"#000000"
            },
          }
        }
        series[match.to].data.push(value)
      }
    }
  }
  result.series=_.toArray(series)
  return result
}

export function generateBasicBarNegative(title, datas, matchs, theme) {
  const data = handleDataBarNegative(datas, matchs)
  const option = {
    backgroundColor: getThemeColor('background_color_echarts', theme),
    title: {
      text: title,
      left:"center",
      textStyle: {
        color: getThemeColor('font_color_echarts_title', theme)
      }
    },
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        type: 'shadow'
      }
    },
    toolbox: {
      feature: {
        saveAsImage: {}
      }
    },
    legend: {
      data: data.legend,
      bottom:10,
      textStyle: {
        color: getThemeColor('font_color_echarts_text', theme)
      }
    },
    toolbox: {
      feature: {
        saveAsImage: {}
      }
    },
    grid: {
      top: '60px',
      left: '2%',
      right: '2%',
      bottom: '40px',
      containLabel: true
    },
    xAxis: {
      type: 'value',
      position: 'top',
      splitArea: { show: false },
      splitLine: {
        lineStyle: {
          color: getThemeColor('font_color_echarts_line', theme)
        }
      },
      axisLabel: {
        textStyle: {
          color: getThemeColor('font_color_echarts_text', theme)
        }
      },
      axisLine: {
        lineStyle: {
          color: getThemeColor('font_color_echarts_line', theme)
        }
      }
    },
    yAxis: {
      type: 'category',
      data: data.yAxis,
      axisLabel: {
        textStyle: {
          color: getThemeColor('font_color_echarts_text', theme)
        }
      },
      splitLine: {
        lineStyle: {
          color: getThemeColor('font_color_echarts_line', theme)
        }
      },
      axisLine: {
        lineStyle: {
          color: getThemeColor('font_color_echarts_line', theme)
        }
      }
    },
    series: formatBar(
      data.series,
      'lr'
    )
  }
  return option
}
