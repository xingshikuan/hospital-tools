import { getBoardData } from '../api/board'

import { generateBasicBar } from './bar'
import { generateBasicBarNegative } from './barNegative'
import { generateLine } from './line'
import { generatePie } from './pie'
import { generateScatter } from './scatter'
import { generateScatterPrettier } from './scatterPrettier'
import { generateRadar } from './radar'
import { generateStackingArea } from './stackingArea'
import { generatePareto } from './pareto'
import { generateBoxplot } from './charts-boxplot'
import { generateLineGradient } from './charts-lineGradient'
import { generateGridMultiple } from './charts-gridMultiple'
import { generateBoardCard } from './boardCard'
import { generateGantt } from './charts-gantt'
import { generateGeoSvgLines } from './charts-geo-svg-lines'
import { generateLineBarCountY } from './lineBarCountY'
import { generateLineBarCount } from './lineBarCount'
import { generateTable } from './charts-table'
import { generateLineRadius } from './lineRadius'
import { generateParetoY } from './paretoY'

export default async function ({ id, params, theme, ganttColor }) {
  const response = await getBoardData(id, params)
  const { type, name, datas, matchs } = response.data

  return generate({ type, datas, matchs, name, theme, ganttColor })
}

export function generate({ type, datas, matchs, name, theme = 'light', ganttColor }) {
  let result
  switch (type) {
    case 'bar':
      result = generateBasicBar(name, datas, matchs, theme)
      break
    case 'barNegative':
      result = generateBasicBarNegative(name, datas, matchs, theme)
      break
    case 'pie':
      result = generatePie(name, datas, matchs, theme)
      break
    case 'line':
      result = generateLine(name, datas, matchs, theme)
      break
    case 'lineRadius':
      result = generateLineRadius(name, datas, matchs, theme)
      break
    case 'radar':
      result = generateRadar(name, datas, matchs, theme)
      break
    case 'stackingArea':
      result = generateStackingArea(name, datas, matchs, theme)
      break
    case 'scatter':
      result = generateScatter(name, datas, matchs, theme)
      break
    case 'scatterPrettier':
      result = generateScatterPrettier(name, datas, matchs, theme)
      break
    case 'pareto':
      result = generatePareto(name, datas, matchs, theme)
      break
    case 'paretoY':
      result = generateParetoY(name, datas, matchs, theme)
      break
    case 'boxplot':
      result = generateBoxplot(name, datas, matchs, theme)
      break
    case 'gridMultiple':
      result = generateGridMultiple(name, datas, matchs, theme)
      break
    case 'lineGradient':
      result = generateLineGradient(name, datas, matchs, theme)
      break
    case 'boardCard':
      result = generateBoardCard(name, datas, matchs, theme)
      break;
    case 'gantt':
      result = generateGantt(name, datas, matchs, ganttColor)
      break;
    case 'geoSvgLines':
      result = generateGeoSvgLines(name, datas, matchs, theme)
      break;
    case 'lineBarCount':
      result = generateLineBarCount(name, datas, matchs, theme)
      break;
    case 'table':
      result = generateTable(name, datas, matchs, theme)
      break;
    case 'lineBarCountY':
      result = generateLineBarCountY(name, datas, matchs, theme)
      break;
  }
  return result
}
