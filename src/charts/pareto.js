import _ from 'lodash'
import { colorsBase, formatBar, formatLineColor } from './color'
import { getThemeColor } from '@/styles/variables'

export function handleDataPareto(datas, matchs) {
  const result = {
    legend: [],
    series: [],
    xAxis: [],
    barName: '',
    lineName: ''
  }
  const series = {}
  const legend = {};
  for (const match of matchs) {
    for (const row of datas[match.apiId].data) {
      const value = _.get(row, match.from)
      if (match.name === 'xAxis') {
        result.xAxis.push(value)
      } else if (match.name === 'bar_series') {
        legend[match.to] = true
        if (!series[match.to]) {
          series[match.to] = {
            name: match.to,
            type: 'bar',
            barMaxWidth: '16px',
            data: [],
            label: { show: true, position: "top" },
          }
        }
        series[match.to].data.push(value)
      } else if (match.name === 'line_series') {
        legend[match.to] = true
        if (!series[match.to]) {
          series[match.to] = {
            name: match.to,
            type: 'line',
            data: [],
            smooth: false,
            label: { show: true, position: "top" },
          }
        }
        series[match.to].data.push(value)
      }
    }
  }


  result.series = _.toArray(series)
  result.legend = _.keys(legend)
  return result
}

export function generatePareto(title, datas, matchs, theme) {
  const data = handleDataPareto(datas, matchs)
  const option = {
    backgroundColor: getThemeColor('background_color_echarts', theme),
    color: colorsBase,
    title: {
      text: title,
      left: 'center',
      textStyle: {
        color: getThemeColor('font_color_echarts_title', theme)
      }
    },
    grid: {
      top: '60px',
      left: '2%',
      right: '2%',
      bottom: '40px',
      containLabel: true
    },
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        type: 'shadow'
      }
    }, 
    toolbox: {
      feature: {
        saveAsImage: {}
      }
    },
    legend: {
      data: data.legend,
      bottom:10,
      show: true,
      textStyle: {
        color: getThemeColor('font_color_echarts_text', theme)
      }
    },
    toolbox: {
      feature: {
        saveAsImage: {}
      }
    },
    xAxis: {
      type: 'category',
      data: data.xAxis,
      axisLabel: {
        textStyle: {
          color: getThemeColor('font_color_echarts_text', theme)
        }
      },
      splitLine: {
        lineStyle: {
          color: getThemeColor('font_color_echarts_line', theme)
        }
      },
      axisLine: {
        lineStyle: {
          color: getThemeColor('font_color_echarts_line', theme)
        }
      }
    },
    yAxis: [
      {
        type: 'value',
        name: data.lineName,
        splitArea: { show: false },
        axisLabel: {
          textStyle: {
            color: getThemeColor('font_color_echarts_text', theme)
          }
        },
        splitLine: {
          lineStyle: {
            color: getThemeColor('font_color_echarts_line', theme)
          }
        },
        axisLine: {
          lineStyle: {
            color: getThemeColor('font_color_echarts_line', theme)
          }
        }
      },
      {
        type: 'value',
        name: data.barName,
        axisLabel: {
          formatter: '{value}%'
        },
        splitArea: { show: false },
        axisLabel: {
          textStyle: {
            color: getThemeColor('font_color_echarts_text', theme)
          }
        },
        splitLine: {
          lineStyle: {
            color: getThemeColor('font_color_echarts_line', theme)
          }
        },
        axisLine: {
          lineStyle: {
            color: getThemeColor('font_color_echarts_line', theme)
          }
        }
      }
    ],
    series: formatLineColor(formatBar(data.series, 'bt'))
  }

  return option
}
