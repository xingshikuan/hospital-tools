import _ from 'lodash'
import { colorsBase } from './color'
import { getThemeColor } from '@/styles/variables'

export function handleDataGridMultiple(datas, matchs) {
  let result = {
    legend: [],
    series: [],
    xAxis: [],
  }
  let legend = {}
  let series = {}
  for (let match of matchs) {
    if (match.name == 'xAxis') {
      for (let row of datas[match.apiId].data) {
        let xAxisValue = _.get(row, match.from)
        result.xAxis.push(xAxisValue)
      }
    } else if (match.name == 'series-up') {
      for (let row of datas[match.apiId].data) {
        let seriesUpValue = _.get(row, match.from)
        if (_.has(legend, match.to)) {
          series[match.to].data.push(seriesUpValue)
        } else {
          legend[match.to] = true
          series[match.to] = {
            name: match.to,
            type: 'line',
            // prettier-ignore
            data: [seriesUpValue]
          }
        }
      }
    } else if (match.name == 'series-down') {
      for (let row of datas[match.apiId].data) {
        let seriesDownValue = _.get(row, match.from)
        if (_.has(legend, match.to)) {
          series[match.to].data.push(seriesDownValue)
        } else {
          legend[match.to] = true
          series[match.to] = {
            name: match.to,
            type: 'line',
            xAxisIndex: 1,
            yAxisIndex: 1,
            // prettier-ignore
            data: [seriesDownValue]
          }
        }
      }
    }
  }
  result.legend = _.keys(legend)
  result.series = _.toArray(series)
  return result
}

export function generateGridMultiple(title, datas, matchs,theme) {
  const data = handleDataGridMultiple(datas, matchs)

  // prettier-ignore

  const option = {
    backgroundColor: getThemeColor('background_color_echarts', theme),
    color: colorsBase,
    title: {
      text: title,
      left: 'center',
      textStyle: {
        color: getThemeColor('font_color_echarts_title', theme)
      }
    },
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        animation: false
      }
    },
    legend: {
      // data: ['Evaporation', 'Rainfall'],
      // left: 10
      show: false,
    },
    toolbox: {
      feature: {
        saveAsImage: {}
      }
    },
    axisPointer: {
      link: [
        {
          xAxisIndex: 'all'
        }
      ]
    },
    dataZoom: [
      {
        show: true,
        realtime: true,
        xAxisIndex: [0, 1]
      },
      {
        type: 'inside',
        realtime: true,
        xAxisIndex: [0, 1]
      }
    ],
    grid: [
      {
        left: 60,
        right: 50,
        height: '35%'
      },
      {
        left: 60,
        right: 50,
        top: '55%',
        height: '35%'
      }
    ],
    xAxis: [
      {
        type: 'category',
        boundaryGap: false,
        axisLine: { onZero: true },
        data: data.xAxis,
        axisLabel: {
          textStyle: {
            color: getThemeColor('font_color_echarts_text', theme)
          }
        },
        splitLine: {
          lineStyle: {
            color: getThemeColor('font_color_echarts_line', theme)
          }
        },
        axisLine: {
          lineStyle: {
            color: getThemeColor('font_color_echarts_line', theme)
          }
        }
      },
      {
        gridIndex: 1,
        type: 'category',
        boundaryGap: false,
        axisLine: { onZero: true },
        data: data.xAxis,
        position: 'top',
        axisLabel: {
          textStyle: {
            color: getThemeColor('font_color_echarts_text', theme)
          }
        },
        splitLine: {
          lineStyle: {
            color: getThemeColor('font_color_echarts_line', theme)
          }
        },
        axisLine: {
          lineStyle: {
            color: getThemeColor('font_color_echarts_line', theme)
          }
        }
      }
    ],
    yAxis: [
      {
        // name: 'Evaporation(m³/s)',
        type: 'value',
        //  max: 500,
        splitArea: { show: false },
        axisLabel: {
          textStyle: {
            color: getThemeColor('font_color_echarts_text', theme)
          }
        },
        splitLine: {
          lineStyle: {
            color: getThemeColor('font_color_echarts_line', theme)
          }
        },
        axisLine: {
          lineStyle: {
            color: getThemeColor('font_color_echarts_line', theme)
          }
        }
      },
      {
        gridIndex: 1,
        //name: 'Rainfall(mm)',
        type: 'value',
        inverse: true,
        splitArea: { show: false },
        axisLabel: {
          textStyle: {
            color: getThemeColor('font_color_echarts_text', theme)
          }
        },
        splitLine: {
          lineStyle: {
            color: getThemeColor('font_color_echarts_line', theme)
          }
        },
        axisLine: {
          lineStyle: {
            color: getThemeColor('font_color_echarts_line', theme)
          }
        }
      }
    ],
    series: data.series
  };
  return option
}


/**
 * Parse the time to string
 * @param {(Object|string|number)} time
 * @param {string} cFormat
 * @returns {string | null}
 */
function parseTime(time, cFormat) {
  if (arguments.length === 0 || !time) {
    return null
  }
  const format = cFormat || '{y}-{m}-{d} {h}:{i}:{s}'
  let date
  if (typeof time === 'object') {
    date = time
  } else {
    if ((typeof time === 'string')) {
      if ((/^[0-9]+$/.test(time))) {
        // support "1548221490638"
        time = parseInt(time)
      } else {
        // support safari
        // https://stackoverflow.com/questions/4310953/invalid-date-in-safari
        time = time.replace(new RegExp(/-/gm), '/')
      }
    }

    if ((typeof time === 'number') && (time.toString().length === 10)) {
      time = time * 1000
    }
    date = new Date(time)
  }
  const formatObj = {
    y: date.getFullYear(),
    m: date.getMonth() + 1,
    d: date.getDate(),
    h: date.getHours(),
    i: date.getMinutes(),
    s: date.getSeconds(),
    a: date.getDay()
  }
  const time_str = format.replace(/{([ymdhisa])+}/g, (result, key) => {
    const value = formatObj[key]
    // Note: getDay() returns 0 on Sunday
    if (key === 'a') { return ['日', '一', '二', '三', '四', '五', '六'][value] }
    return value.toString().padStart(2, '0')
  })
  return time_str
}

