import _ from 'lodash'
import { colorsBase } from './color'
import { getThemeColor } from '@/styles/variables'
export function handleDataLine(datas, matchs) {
  let result = {
    legend: [],
    series: [],
    xAxis: []
  }
  let legend = {}
  let series = {}
  let customColor = []
  // 颜色索引
  let colorIndex = 0
  for (let match of matchs) {
    if (match.name == 'xAxis') {
      for (let row of datas[match.apiId].data) {
        let xAxisValue = _.get(row, match.from)
        result.xAxis.push(xAxisValue)
      }
    } else if (match.name == 'series') {
      for (let row of datas[match.apiId].data) {
        let seriesValue = _.get(row, match.from)
        if (_.has(legend, match.to)) {
          series[match.to].data.push(seriesValue)
        } else {
          legend[match.to] = true
          series[match.to] = {
            name: match.to,
            type: 'line',
            smooth: false,
            label: { show: true },
            data: [seriesValue]
          }

          if (match.color) {
            series[match.to].lineStyle = {
              color: match.color
            }
            series[match.to].color = match.color
            customColor.push({ ...match, color: match.color })
          } else {
            series[match.to].lineStyle = {
              color: colorsBase[colorIndex % colorsBase.length]
            }
            series[match.to].color = colorsBase[colorIndex % colorsBase.length]
            customColor.push({
              ...match,
              color: colorsBase[colorIndex % colorsBase.length]
            })
            colorIndex++
          }
        }
      }
    }
  }
  result.legend = _.keys(legend)
  result.series = _.toArray(series)
  result.customColor = customColor
  return result
}

export function generateLine(title, datas, matchs, theme) {
  let data = handleDataLine(datas, matchs)
  const option = {
    backgroundColor: getThemeColor('background_color_echarts', theme),
    color: colorsBase,
    title: {
      text: title,
      left:"center",
      textStyle: {
        color: getThemeColor('font_color_echarts_title', theme)
      }
    },
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        // 坐标轴指示器，坐标轴触发有效
        type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
      }
    }, 
    toolbox: {
      feature: {
        saveAsImage: {}
      }
    },
    legend: {
      data: data.legend,
      bottom:10,
      textStyle: {
        color: getThemeColor('font_color_echarts_text', theme)
      }
    },
    toolbox: {
      feature: {
        saveAsImage: {}
      }
    },
    grid: {
      top: '60px',
      left: '2%',
      right: '2%',
      bottom: '40px',
      containLabel: true
    },
    xAxis: {
      type: 'category',
      boundaryGap: false,
      data: data.xAxis,
      axisLabel: {
        textStyle: {
          color: getThemeColor('font_color_echarts_text', theme)
        }
      },
      splitLine: {
        lineStyle: {
          color: getThemeColor('font_color_echarts_line', theme)
        }
      },
      axisLine: {
        lineStyle: {
          color: getThemeColor('font_color_echarts_line', theme)
        }
      }
    },
    yAxis: {
      type: 'value',
      splitArea: { show: false },
      axisLabel: {
        textStyle: {
          color: getThemeColor('font_color_echarts_text', theme)
        }
      },
      splitLine: {
        lineStyle: {
          color: getThemeColor('font_color_echarts_line', theme)
        }
      },
      axisLine: {
        lineStyle: {
          color: getThemeColor('font_color_echarts_line', theme)
        }
      }
    },
    series: data.series,
    customColor: data.customColor
  }
  return option
}
