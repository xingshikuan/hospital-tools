import _ from 'lodash'
import { colorsBase, hex2Rgb } from './color';

export function handleDataBoardCard(datas, matchs) {

    let series = []

    for (let j = 0; j < matchs.length; j++) {
        const match = matchs[j]
        for (let i = 0; i < datas[match.apiId].data.length; i++) {
            const row = datas[matchs[j].apiId].data[i];

            if (!series[i]) {
                series[i] = {
                    trendData:[]
                }
            }
            if (match.name == 'trendData' || match.name == 'trendType') {

                const from = match.from.split('.');
                for (let k = 0; k < from.length; k++) {
                    let d = _.get(row, from[k]);
                    if (Array.isArray(d)) {
                        for (let n = 0; n < d.length; n++) {
                            if (!series[i].trendData[n]) {
                                series[i].trendData[n] = {};
                            }
                            series[i].trendData[n][matchs[j].name] = _.get(d[n], from[k + 1]);
                        }
                    }
                }
            } else {
                series[i][match.name] = _.get(row, match.from);
            }

        }
    }
    return {
        series: series.map((_,i) => {
            const color = colorsBase[i % colorsBase.length]
            _.color = [color, hex2Rgb(color, 0.1), hex2Rgb(color, 0)]
            return _;
        }),
        type: "boardCard",
    }
}

export function generateBoardCard(title, datas, matchs) {
    return handleDataBoardCard(datas, matchs)
}