import _ from 'lodash'
import { colorsBase } from './color'
import { getThemeColor } from '@/styles/variables'
export function handleDataScatterPrettier(datas, matchs) {
  const result = {
    legend: [],
    data: [],
    x_unit: '',
    y_unit: ''
  }
  const legend = {}
  const series = {}
  for (const match of matchs) {
    legend[match.to] = true;
    if (!series[match.to]) {
      series[match.to] = {
        name: match.to,
        type: 'scatter',
        emphasis: {
          focus: 'series'
        },
        // prettier-ignore
        data: _.get(datas[match.apiId].data, match.from),
        markArea: {
          silent: true,
          itemStyle: {
            color: 'transparent',
            borderWidth: 1,
            borderType: 'dashed'
          },
          data: [
            [
              {
                name: match.to,
                xAxis: 'min',
                yAxis: 'min',
                "label": {
                  show: false,
                },
              },
              {
                xAxis: 'max',
                yAxis: 'max',
                "label": {
                  show: false,
                },
              }
            ]
          ]
        },
        markPoint: {
          data: [
            {
              type: 'max', name: 'Max',
              "label": {
                show: false
              }
            },
            {
              type: 'min', name: 'Min',
              "label": {
                show: false
              }
            }
          ]
        },
        markLine: {
          lineStyle: {
            type: 'solid'
          },
          data: [
            {
              type: 'average', name: 'AVG', xAxis: "average", "label": {
                show: false
              }
            },
            {
              type: 'average', name: 'AVG', yAxis: "average", "label": {
                show: false
              }
            },
          ]
        }
      }
    }
    // for (let i = 0; i < datas[match.apiId].data.length; i++) {
    //   const item = datas[match.apiId].data[i]
    //   let value = _.get(item, match.from);

    //   if (!series[match.to].data[i]) {
    //     series[match.to].data[i] = [];
    //   }
    //   if (match.name == 'xAxis') {
    //     series[match.to].data[i][0] = value
    //   } else if (match.name == 'yAxis') {
    //     series[match.to].data[i][1] = value
    //   }
    // }
  }

  result.legend = _.keys(legend)
  result.series = _.toArray(series)
  return result
}

export function generateScatterPrettier(title, datas, matchs, theme) {
  const result = handleDataScatterPrettier(datas, matchs)
  const option = {
    backgroundColor: getThemeColor('background_color_echarts', theme),
    color: colorsBase,
    title: {
      text: title,
      left:"center",
      textStyle: {
        color: getThemeColor('font_color_echarts_title', theme)
      }
    },
    toolbox: {
      feature: {
        saveAsImage: {}
      }
    },
    grid: {
      top: '60px',
      left: '2%',
      right: '2%',
      bottom: '40px',
      containLabel: true
    },
    toolbox: {
      feature: {
        saveAsImage: {}
      }
    },
    tooltip: {
      // trigger: 'axis',
      showDelay: 0,
      formatter: function (params) {
        if (params.value.length > 1) {
          return (
            params.name +
            ' :<br/>' +
            params.value[0] +
            params.value[2] +
            ' ' +
            params.value[1] +
            params.value[3]
          )
        } else {
          return params.name + ' : ' + params.value
        }
      },
      axisPointer: {
        show: true,
        type: 'cross',
        lineStyle: {
          type: 'dashed',
          width: 1
        }
      }
    },
    legend: {
      data: result.legend,
      left: 'center',
      bottom: 10,
      textStyle: {
        color: getThemeColor('font_color_echarts_text', theme)
      }
    },
    xAxis: [
      {
        type: 'value',
        scale: true,
        splitLine: {
          show: false
        },
        splitArea: { show: false },
        axisLabel: {
          textStyle: {
            color: getThemeColor('font_color_echarts_text', theme)
          }
        },
        // splitLine: {
        //   lineStyle: {
        //     color: getThemeColor('font_color_echarts_line', theme)
        //   }
        // },
        axisLine: {
          lineStyle: {
            color: getThemeColor('font_color_echarts_line', theme)
          }
        }
      }
    ],
    yAxis: [
      {
        type: 'value',
        scale: true,
        splitLine: {
          show: false
        },
        splitArea: { show: false },
        axisLabel: {
          textStyle: {
            color: getThemeColor('font_color_echarts_text', theme)
          }
        },
        // splitLine: {
        //   lineStyle: {
        //     color: getThemeColor('font_color_echarts_line', theme)
        //   }
        // },
        axisLine: {
          lineStyle: {
            color: getThemeColor('font_color_echarts_line', theme)
          }
        }
      }
    ],
    series: result.series
  }
  return option
}